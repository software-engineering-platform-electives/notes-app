# Notes App

Note-taking app for Platform Elective

## Requirements

npm install

## How to run

node app.js --help

## Add a new note

node app.js add --title="Add Note" --body="This adds a new note."

## Remove a note

node app.js remove --title="Add Note"

## Read a note

node app.js read --title="Course ideas"

## List the notes

node app.js list